package com.example.nooob;

import org.apache.http.HttpRequest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class NooobApplication {

    public static void main(String[] args) { SpringApplication.run(NooobApplication.class, args); }

}
