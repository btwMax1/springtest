package com.example.nooob.services.userService;

import com.example.nooob.services.HttpClientService;
import com.example.nooob.services.userService.dto.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService {
    private final HttpClientService httpClientService;

    UserService(HttpClientService httpClientService) {

        this.httpClientService = httpClientService;
    }

    public List<User> getUsers (Integer limit) {
        String url = "https://jsonplaceholder.typicode.com/users";
        url += "?_limit=";
        url += limit;
        String response = httpClientService.get(url);

        ObjectMapper mapper = new ObjectMapper();
        try {
            List<User> users = mapper.readValue(response, new TypeReference<List<User>>(){});
            return users;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
