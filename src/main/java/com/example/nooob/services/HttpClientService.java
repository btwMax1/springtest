package com.example.nooob.services;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class HttpClientService {
    final CloseableHttpClient httpclient = HttpClients.createDefault();


    public String get(String url) {
        try {
            HttpUriRequest httpGet = new HttpGet(url);
            try (CloseableHttpResponse response1 = httpclient.execute(httpGet)) {
                final HttpEntity entity1 = response1.getEntity();
                return EntityUtils.toString(entity1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
