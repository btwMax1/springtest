package com.example.nooob.controllers;

import com.example.nooob.services.userService.UserService;
import com.example.nooob.services.userService.dto.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController("/test")
public class TestController {

    private final UserService userService;

    TestController(UserService userService) {

        this.userService = userService;
    }

    @GetMapping("/users")
    public List<User> getUsers(
            @RequestParam(value = "limit", defaultValue = "10", required = false) Integer limit
    ) {

        return userService.getUsers(limit);
    }
}
